# Python for Data Science - Nano Course

## Playlist
- [Course Youtube playlist](s.id/mg-ds-nano)

## Outline

### Data Science Introduction
- [Hello Data !](https://youtube.com)
- [Mengapa belajar Data Science ?](./)
- [Aneka profesi terkait Data Science](./)
- [Aneka teknologi Data Science](./)
---

### Google Colab
- [Apa sih Google Colab itu ?]()
- [Instalasi di Google Drive]()
- [Membuat file baru]()
- [Mengakses file dan folder]()
- [Mengakses Linux command line]()
- [Instalasi package Python]()

### Basic Programming with Python
- [Variable - simpan nilai di memory]()
- [Data type - perlakukan variable sesuai tipe datanya]()
- [Operator - hubungan antar variable]()
- [Decision - ambil keputusan berdasarkan kondisi]()
- [Loops - ulang perintah hingga kondisi terpenuhi]()
- [Function - wadah serangkaian perintah]()
- [Lambda - function singkat sebaris]()
- [Class & Object - entitas dengan tanggung jawab tertentu]()
- [Dictionary - pasangan kata kunci dan nilai]()
- [List - variable berisi sekumpulan data]()
- [Module - sekumpulan class dan function dengan tugas tertentu]()
- [Try & Except - penanganan perintah error]()

### Data Sources
- []()
- []()
- []()
- []()
- []()
- []()
- []()
- []()

### Database and SQL
- []()
- []()
- []()
- []()
- []()
- []()
- []()
- []()

### Data Processing with Pandas
- []()
- []()
- []()
- []()
- []()
- []()
- []()
- []()

### Data Visualization
- []()
- []()
- []()
- []()
- []()
- []()
- []()
- []()

### Statistics
- []()
- []()
- []()
- []()
- []()
- []()
- []()
- []()

### Machine Learning
- []()
- []()
- []()
- []()
- []()
- []()
- []()
- []()
