# Python for Data Science - Nano Course

## Outline

### Data Science Introduction
- [Google Colab - Hello Data Science](https://colab.research.google.com/drive/1CMlR2I13DzDZSu6qYidy-CgDLQvWCzkN?usp=sharing)
---

### Google Colabs
- [Gitlab - Cara instalasi](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab)
---

### Basic Programming with Python
- [Tutorialspoint - Python](https://www.tutorialspoint.com/python/index.htm)
---

### Data Sources
Sources:
- People
- Web API 
- Web document - scraping : requests, selenium, beautifulsoup
- Real time data 
- Database : 
- Dataset library
Formats:
- Text document: CSV, JSON, textfile, XML
- Office document: xlsx, pdf
- Media: voice, video, image, animation
- Database: relational (MySQL, PostgreSQL), non relational (MongoDB, DGraph)
---

### Database and SQL
- relational : MySQL, SQL Server, PostgreSQL > Advanced Query
```sql
SELECT nama, alamat, jurusan -- Ambil kolom nama, alamat, jurusan
FROM mahasiswa -- Dari data mahasiswa
WHERE ipk > 3.99 -- yang IPK > 3.99
AND alamat LIKE '%panyileukan%' -- alamatnya mengandung panyileukan
```
- non relational
---

### Data Processing with Pandas
- [Google Colab - Data Processing with Pandas](https://colab.research.google.com/drive/1WrnwEpbcmH-Dv8JVygTi85NkTfiC_SBm?usp=sharing)
---

### Data Visualization
- Py: Matplotlib
- Py: Seaborn
- Js: D3
---

### Statistics
- [Google Colab - Statistics](https://colab.research.google.com/drive/1leW-NHAZnpn_fD9w93NRmuH8pCWZLu3y?usp=sharing)
---

### Machine Learning
- EPT (Experience, Performance, Task)
    - Menggunakan Experience untuk memaksimalkan Performance dari sebauh Task
- Data science - Scikit learn
    - AutoML : PyCaret
    - ML Ops 
- AI - Deep learning - Keras, Tensorflow, Pytorch
    - Discriminative
    - Generative

---

https://chat.whatsapp.com/KrvB8dvprMLBkGHBre7jAF
